package Request_Specification_Response_Class;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class Patch {

	public static void main(String[] args) {
		// Step 1 : Collect all needed information and save it into local variables

				String req_body = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";

				String hostname = "https://reqres.in";

				String resource = "/api/users/2";

				String headername = "Content-Type";

				String headervalue = "application/json";

				// Step 2 : Build the request specification using RequestSpecification class

				RequestSpecification requestSpec = RestAssured.given();

				// Step 2.1 : Set request header

				requestSpec.header(headername, headervalue);

				// Step 2.2 : Set request body

				requestSpec.body(req_body);

				// Step 3 : Send the API request

				Response response = requestSpec.patch(hostname + resource);
				
				System.out.println(response.getBody().asString());
				
				// Fetch the status code and store into local variable
				
				int status_code = response.statusCode();
				System.out.println(response.statusCode());
				
				
				// Step 4 : create jsonpath object for parsing
				
				JsonPath jsp_req = new JsonPath(req_body);
				String req_name = jsp_req.getString("name");
				String req_job = jsp_req.getString("job");
				
				
				// Step 5 : generate current date
				
				LocalDateTime  currentDate = LocalDateTime.now();
		        String expectedDate = currentDate.toString().substring(0, 11);
		        
				
				// Step 6 : Using TestNG for validation
		        Assert.assertEquals(status_code, 200);
				
				Assert.assertEquals(jsp_req.getString("name"),response.jsonPath().getString("name"));
				
				Assert.assertEquals(jsp_req.getString("job"),response.jsonPath().getString("job"));
				
				Assert.assertEquals(expectedDate,response.jsonPath().getString("updatedAt").substring(0,11));
					
			}
	}

