package Request_Specification_Response_Class;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class delete_Api {
	public static void main(String[] args) {
		
		// step 1: collect all information for configuring API

		String hostname = "https://reqres.in/";

		String resource = "api/users/2";

		String headername = "Content-Type";

		String headervalue = "application/json";

		// step 2 Build the request specification using request specification class

		RequestSpecification requestSpec = RestAssured.given();

		Response response = requestSpec.delete(hostname + resource);

		System.out.println(response.statusCode());
	}
	
	
}

