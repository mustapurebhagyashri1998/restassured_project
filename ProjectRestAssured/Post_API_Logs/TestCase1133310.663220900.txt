Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Vaishnavi",
    "job": "qalead"
}

Response header date is : 
Wed, 28 Feb 2024 08:03:10 GMT

Response body is : 
{"name":"Vaishnavi","job":"qalead","id":"155","createdAt":"2024-02-28T08:03:10.589Z"}